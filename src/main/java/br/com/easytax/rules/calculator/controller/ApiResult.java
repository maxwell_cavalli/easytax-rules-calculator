package br.com.easytax.rules.calculator.controller;

import br.com.easytax.iamlib.controller.CrudApi;
import br.com.easytax.iamlib.security.Privilege;
import br.com.easytax.iamlib.security.SecuredContext;
import br.com.easytax.rules.calculator.domain.Result;
import br.com.easytax.rules.calculator.domain.RuleResult;
import br.com.easytax.rules.calculator.service.ResultService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/result")
@SecuredContext(context = "Result")
public class ApiResult extends CrudApi<Result> {

    public static final String NOT_ALLOWED = "Not Allowed";

    @Privilege(actions = {"read-result"})
    @GetMapping(path = "/request-id/{id}")
    public ResponseEntity<RuleResult> readResult(@PathVariable String id) {
        return ((ResultService)service).getByRequestId(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    @Override
    public ResponseEntity<?> create(@Valid Result result, HttpServletRequest request) {
        return ResponseEntity.badRequest().body(NOT_ALLOWED);
    }

    @Override
    public ResponseEntity<?> delete(String id) {
        return ResponseEntity.badRequest().body(NOT_ALLOWED);
    }

    @Override
    public ResponseEntity<?> put(String id, Result current) {
        return ResponseEntity.badRequest().body(NOT_ALLOWED);
    }
}
