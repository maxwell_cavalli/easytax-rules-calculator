package br.com.easytax.rules.calculator.controller;

import br.com.easytax.iamlib.controller.CrudApi;
import br.com.easytax.iamlib.security.Privilege;
import br.com.easytax.iamlib.security.SecuredContext;
import br.com.easytax.iamlib.security.SecurityHelper;
import br.com.easytax.rules.calculator.domain.Rule;
import br.com.easytax.rules.calculator.domain.RuleResult;
import br.com.easytax.rules.calculator.domain.receiver.TaxParam;
import br.com.easytax.rules.calculator.service.RuleService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/rule")
@SecuredContext(context = "Rule")
public class ApiRule extends CrudApi<Rule> {

    @Privilege(actions = {"upload"})
    @PostMapping(path = "/{id}/upload")
    public ResponseEntity<Void> uploadScript(@RequestParam("file") MultipartFile file,
                                          @PathVariable String id) {

        ((RuleService) service).uploadScript(id, file);
        return ResponseEntity.ok().build();
    }

    @Privilege(actions = {"download"})
    @GetMapping(path = "/{id}/download")
    public byte[] downloadScript(@PathVariable String id) {
        return ((RuleService) service).downloadScript(id);
    }

    @Privilege(actions = {"validate"})
    @GetMapping(path = "/{id}/validate")
    public ResponseEntity<Boolean> validateScript(@PathVariable String id) {
        return ResponseEntity.ok(((RuleService) service).validate(id));
    }

    @Privilege(actions = {"validate-params"})
    @PostMapping(path = "/validate-params")
    public ResponseEntity<?> validateParams(@RequestBody TaxParam taxParam) {
        return Optional.ofNullable(((RuleService) service).loadAndValidateParams(taxParam))
                .map(validationMessages -> validationMessages.isEmpty()
                        ? ResponseEntity.ok().body(Boolean.TRUE)
                        : ResponseEntity.badRequest().body(validationMessages))
                .orElse(ResponseEntity.badRequest().build());
    }

    @Privilege(actions = {"execute-rule"})
    @PostMapping(path = "/execute-rule")
    public ResponseEntity<RuleResult> executeRule(@RequestBody TaxParam taxParam) {
        taxParam.setUserId(SecurityHelper.getCurrentUser().getId());
        taxParam.setRequestId(null);

        return ((RuleService) service).loadAndExecuteRule(taxParam)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }



}
