package br.com.easytax.rules.calculator.domain;

import br.com.easytax.iamlib.domain.RealmIsolatedDomain;
import br.com.easytax.rules.calculator.domain.receiver.ResultStatus;
import br.com.easytax.rules.calculator.domain.receiver.TaxParam;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Data
@Document("results")
@CompoundIndex(def = "{'id': 1}")
public class Result extends RealmIsolatedDomain {

    private ResultParam resultParam;
    private String ruleId;
    private String requestId;
    private String userId;
    private String customerId;
    private String rawResult;
    private Set<String> validations;
    private ResultStatus status;

    @Override
    public boolean isRealmIdOptional() {
        return true;
    }

    public static Result of(final TaxParam taxParam) {
        final Result result = new Result();
        result.setResultParam(ResultParam.of(taxParam));
        result.setRequestId(
                StringUtils.isBlank(taxParam.getRequestId())
                        ? UUID.randomUUID().toString()
                        : taxParam.getRequestId()
        );

        result.setUserId(taxParam.getUserId());
        result.setCustomerId(taxParam.getCustomerId());
        return result;
    }

    public Map getResultObject() {
        if (StringUtils.isNotBlank(rawResult)) {
            final ObjectMapper objectMapper = new ObjectMapper();
            try {
                return objectMapper.readValue(rawResult, Map.class);
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
            }
        }

        return Collections.emptyMap();
    }

}
