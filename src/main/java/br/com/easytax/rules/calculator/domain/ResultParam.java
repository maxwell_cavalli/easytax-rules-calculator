package br.com.easytax.rules.calculator.domain;

import br.com.easytax.rules.calculator.domain.receiver.TaxParam;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
public class ResultParam {

    private String city;
    private String serviceCode;
    private Date documentDate;
    private List<ResultParamDetail> details;

    public static ResultParam of(final TaxParam taxParam) {
        final ResultParam resultParam = new ResultParam();
        resultParam.setCity(taxParam.getCity());
        resultParam.setServiceCode(taxParam.getServiceCode());
        resultParam.setDocumentDate(taxParam.getDocumentDate());

        if (taxParam.getDetails() != null) {
            resultParam.setDetails(
                    taxParam.getDetails().stream()
                            .map(ResultParamDetail::of)
                            .collect(Collectors.toList())
            );
        }

        return resultParam;
    }

}
