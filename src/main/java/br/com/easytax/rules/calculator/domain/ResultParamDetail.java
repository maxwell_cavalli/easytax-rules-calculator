package br.com.easytax.rules.calculator.domain;

import br.com.easytax.rules.calculator.domain.receiver.TaxParamDetail;
import lombok.Data;

@Data
public class ResultParamDetail {

    private String name;
    private String stringValue;
    private Double doubleValue;
    private Integer integerValue;

    public static ResultParamDetail of(final TaxParamDetail taxParamDetail) {
        final ResultParamDetail resultParamDetail = new ResultParamDetail();

        resultParamDetail.setName(taxParamDetail.getName());
        resultParamDetail.setStringValue(taxParamDetail.getStringValue());
        resultParamDetail.setDoubleValue(taxParamDetail.getDoubleValue());
        resultParamDetail.setIntegerValue(taxParamDetail.getIntegerValue());
        return resultParamDetail;

    }

}
