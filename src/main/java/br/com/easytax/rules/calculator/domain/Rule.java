package br.com.easytax.rules.calculator.domain;

import br.com.easytax.iamlib.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Document("rules")
@CompoundIndex(def = "{'id': 1}")
public class Rule extends BaseDomain {

    @NotBlank(message = "is required")
    private String name;

    @JsonIgnore
    private String script;

    private Integer version;

    @NotNull(message = "is required")
    private String city;

    @NotBlank(message = "is required")
    private String serviceId;

    @NotNull(message = "is required")
    private Date startDate;

    @NotNull(message = "is required")
    private Date endDate;

    @NotNull(message = "is required")
    @Valid
    private List<RuleParameters> parameters;

    private boolean active;

    @JsonProperty("script")
    private String scriptReturn;

    @Override
    public void fillNew() {
        super.fillNew();
        this.version = 1;
        this.active = true;
    }
}
