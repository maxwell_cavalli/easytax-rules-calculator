package br.com.easytax.rules.calculator.domain;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RuleParameters {

    @NotBlank(message = "is required")
    private String name;

    @NotBlank(message = "is required")
    private String type;

    private String value;

}
