package br.com.easytax.rules.calculator.domain;

import br.com.easytax.iamlib.domain.BaseDomain;
import br.com.easytax.rules.calculator.domain.receiver.ResultStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;
import java.util.Set;

@Data
@AllArgsConstructor
public class RuleResult extends BaseDomain {

    private String requestId;
    private Map resultData;
    private Set<String> validations;
    private ResultStatus status;

}
