package br.com.easytax.rules.calculator.domain.customer;

import lombok.Data;
import lombok.ToString;

import java.util.Set;

@Data
@ToString
public class Customer {

    private String id;
    private String companyName;
    private String tradeName;
    private Set<String> serviceTax;
    private Set<String> cityCode;
    private String realmId;

}
