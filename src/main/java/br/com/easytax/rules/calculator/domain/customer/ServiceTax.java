package br.com.easytax.rules.calculator.domain.customer;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class ServiceTax {

    private String id;
    private String code;
    private String name;

}
