package br.com.easytax.rules.calculator.domain.receiver;

public enum ResultStatus {
    SUCCESS,
    FAIL
}
