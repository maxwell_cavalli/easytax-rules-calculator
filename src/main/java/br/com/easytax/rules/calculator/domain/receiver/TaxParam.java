package br.com.easytax.rules.calculator.domain.receiver;

import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@ToString
@Data
public class TaxParam {
    private String requestId;

    private String city;

    private String serviceCode;

    private Date documentDate;

    private List<TaxParamDetail> details;

    private String userId;

    private String customerId;

}
