package br.com.easytax.rules.calculator.domain.receiver;

import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@ToString
@Data
public class TaxParamDetail {

    private String name;

    private String stringValue;
    private Double doubleValue;
    private Integer integerValue;

    public boolean noValue() {
        return StringUtils.isBlank(stringValue) &&
                Objects.isNull(doubleValue) &&
                Objects.isNull(integerValue);
    }

    public Map toMap() {
        Map ret = new HashMap();
        ret.put(this.name, this.getValue());

        return ret;
    }

    private Object getValue() {
        if (StringUtils.isNotBlank(stringValue)) {
            return stringValue;
        } else if (doubleValue != null) {
            return doubleValue;
        } else if (integerValue != null) {
            return integerValue;
        }

        return null;
    }


}
