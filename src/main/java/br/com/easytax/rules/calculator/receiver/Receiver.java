package br.com.easytax.rules.calculator.receiver;

import br.com.easytax.rules.calculator.domain.receiver.TaxParam;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;


@Slf4j
@AllArgsConstructor
@Component
public class Receiver {

    private final ObjectMapper objectMapper;

    public void receiveMessage(final String message) {
        if (StringUtils.isNotBlank(message)) {

            try {
                TaxParam taxParam = objectMapper.readValue(message, TaxParam.class);
                log.info(taxParam.toString());
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
            }

        }
    }
}
