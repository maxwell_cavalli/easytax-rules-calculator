package br.com.easytax.rules.calculator.repository;

import br.com.easytax.iamlib.repository.BaseRepository;
import br.com.easytax.rules.calculator.domain.Result;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends BaseRepository<Result> {

}
