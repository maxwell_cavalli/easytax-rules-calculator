package br.com.easytax.rules.calculator.service;

import br.com.easytax.rules.calculator.domain.Rule;
import br.com.easytax.rules.calculator.domain.RuleParameters;
import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.groovy.control.CompilationFailedException;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Slf4j
@Service
public class GroovyExecutorService {

    private static Random random = new Random();

    public boolean validate(final Rule rule) {

        if (StringUtils.isBlank(rule.getScript())) {
            throw new IllegalArgumentException("Invalid script");
        }

        try {
            GroovyClassLoader loader = new GroovyClassLoader(this.getClass().getClassLoader());
            GroovyShell shell = new GroovyShell(loader, new Binding());

            String strCopy = rule.getScript();

            Map<String, Object> mapParam = new HashMap<>();
            for (RuleParameters parameter : rule.getParameters()) {
                if ("string".equalsIgnoreCase(parameter.getType())) {
                    strCopy = strCopy.replaceAll("var_" + parameter.getName(), "mock_value");
                    mapParam.put(parameter.getName(), "mock_value");
                } else if ("integer".equalsIgnoreCase(parameter.getType())) {
                    int randomNum = random.nextInt((999 - 1) + 1) + 999;
                    strCopy = strCopy.replaceAll("var_" + parameter.getName(), String.valueOf(randomNum));
                    mapParam.put(parameter.getName(), randomNum);
                } else if ("double".equalsIgnoreCase(parameter.getType())) {
                    double randomNum = random.nextDouble();
                    strCopy = strCopy.replaceAll("var_" + parameter.getName(), String.valueOf(randomNum));
                    mapParam.put(parameter.getName(), randomNum);
                }
            }

            if (strCopy.indexOf("var_") > -1){
                throw new IllegalArgumentException("Invalid script or parameter configuration");
            }

            Script script = shell.parse(strCopy);
            Object result = script.invokeMethod("funcao", new Object[] { mapParam });
            log.info("Result of CalcScript.calcSum() method is {}", result);

            return true;
        } catch (CompilationFailedException e) {
            log.error(e.getMessage());
        }

        return false;
    }

    public Map execute(final Rule rule, final Map<String, Object> parameters){
        GroovyClassLoader loader = new GroovyClassLoader(this.getClass().getClassLoader());
        GroovyShell shell = new GroovyShell(loader, new Binding());

        String strCopy = rule.getScript();
        Script script = shell.parse(strCopy);
        return (Map) script.invokeMethod("funcao", new Object[] { parameters });
    }

}
