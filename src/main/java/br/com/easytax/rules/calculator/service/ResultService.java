package br.com.easytax.rules.calculator.service;

import br.com.easytax.iamlib.exception.DataNotFoundException;
import br.com.easytax.iamlib.service.BaseService;
import br.com.easytax.rules.calculator.domain.Result;
import br.com.easytax.rules.calculator.domain.RuleResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class ResultService extends BaseService<Result> {

    public Optional<RuleResult> getByRequestId(final String requestId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("requestId").is(requestId));

        Optional<Result> result = get(query);
        return Optional.ofNullable(
                result
                        .map(this::createRuleResult)
                        .orElseThrow(() -> new DataNotFoundException("RequestId not found"))
        );
    }

    private RuleResult createRuleResult(final Result result1) {
        return new RuleResult(result1.getRequestId(), result1.getResultObject(), result1.getValidations(), result1.getStatus());
    }


}


