package br.com.easytax.rules.calculator.service;

import br.com.easytax.iamlib.domain.User;
import br.com.easytax.iamlib.exception.BadRequestException;
import br.com.easytax.iamlib.security.SecurityHelper;
import br.com.easytax.iamlib.service.BaseService;
import br.com.easytax.iamlib.service.PageImpl;
import br.com.easytax.iamlib.service.authorization.AuthServiceAPI;
import br.com.easytax.rules.calculator.domain.Result;
import br.com.easytax.rules.calculator.domain.Rule;
import br.com.easytax.rules.calculator.domain.RuleResult;
import br.com.easytax.rules.calculator.domain.address.City;
import br.com.easytax.rules.calculator.domain.customer.Customer;
import br.com.easytax.rules.calculator.domain.customer.ServiceTax;
import br.com.easytax.rules.calculator.domain.receiver.ResultStatus;
import br.com.easytax.rules.calculator.domain.receiver.TaxParam;
import br.com.easytax.rules.calculator.service.address.AddressServiceAPI;
import br.com.easytax.rules.calculator.service.customer.CustomerServiceAPI;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

@Slf4j
@Service
public class RuleService extends BaseService<Rule> {

    public static final String ID_NOT_FOUND = "Id not found";

    private final GroovyExecutorService groovyExecutorService;
    private final ResultService resultService;

    private final CustomerServiceAPI customerServiceAPI;
    private final AddressServiceAPI addressServiceAPI;
    private final AuthServiceAPI authServiceAPI;

    public RuleService(final GroovyExecutorService groovyExecutorService,
                       final ResultService resultService,
                       final CustomerServiceAPI customerServiceAPI,
                       final AddressServiceAPI addressServiceAPI,
                       final AuthServiceAPI authServiceAPI) {
        this.groovyExecutorService = groovyExecutorService;
        this.resultService = resultService;
        this.customerServiceAPI = customerServiceAPI;
        this.addressServiceAPI = addressServiceAPI;
        this.authServiceAPI = authServiceAPI;
    }

    @Override
    public Rule beforeCreate(Rule rule) {
        customerServiceAPI.retrieveServiceById(rule.getServiceId())
                .orElseThrow(() -> new BadRequestException("Service not found"));

        addressServiceAPI.retrieveCityById(rule.getCity())
                .orElseThrow(() -> new BadRequestException("City not found"));

        return super.beforeCreate(rule);
    }

    @Override
    public Rule beforeUpdate(Rule previous, Rule current) {
        current.setServiceId(previous.getServiceId());
        current.setCity(previous.getCity());
        current.setScript(previous.getScript());

        return super.beforeUpdate(previous, current);
    }

    public Set<String> loadAndValidateParams(final TaxParam taxParam) {
        return loadAndValidateParams(taxParam,
                rule -> {
                },
                realmId -> {
                });
    }

    public Set<String> loadAndValidateParams(final TaxParam taxParam,
                                             final Consumer<Rule> ruleConsumer,
                                             final Consumer<String> realIdConsumer) {

        final Set<String> validationMessages = new HashSet<>();
        try {
            final Optional<Rule> rule = loadRule(taxParam, realIdConsumer);
            rule.ifPresent(rule1 -> {
                        ruleConsumer.accept(rule1);

                        rule1.getParameters().forEach(ruleParameters -> {

                            if (taxParam.getDetails().stream()
                                    .noneMatch(taxParamDetail -> taxParamDetail.getName().equals(ruleParameters.getName()))) {
                                validationMessages.add("Missing parameter: " + ruleParameters.getName());
                            } else {
                                taxParam.getDetails().stream()
                                        .filter(taxParamDetail -> taxParamDetail.getName().equals(ruleParameters.getName()))
                                        .findFirst()
                                        .ifPresentOrElse(taxParamDetail -> {
                                                    if (taxParamDetail.noValue()) {
                                                        validationMessages.add("No value for parameter: " + ruleParameters.getName());
                                                    }
                                                },
                                                () -> validationMessages.add("Missing parameter: " + ruleParameters.getName()));

                            }
                        });
                    }
            );
        } catch (BadRequestException e) {
            validationMessages.add(e.getMessage());
        }

        return validationMessages;
    }

    public Optional<RuleResult> loadAndExecuteRule(final TaxParam taxParam) {
        final AtomicReference<Rule> internalRule = new AtomicReference<>();
        final AtomicReference<String> internalRealmId = new AtomicReference<>();

        final Set<String> validations = loadAndValidateParams(taxParam,
                internalRule::set,
                internalRealmId::set);

        if (validations.isEmpty()) {
            return Optional.of(
                    Optional.of(internalRule.get())
                            .map(rule -> executeRule(rule, taxParam, internalRealmId.get()))
                            .orElse(null)
            );
        }

        final String requestId = StringUtils.isBlank(taxParam.getRequestId()) ? UUID.randomUUID().toString() : taxParam.getRequestId();
        if (internalRealmId.get() != null) {
            return saveAndReturnErrorResult(taxParam, internalRule, internalRealmId, validations, requestId);
        }

        return Optional.of(
                new RuleResult(requestId, Collections.emptyMap(),
                        validations, ResultStatus.FAIL)
        );
    }

    private Optional<RuleResult> saveAndReturnErrorResult(final TaxParam taxParam,
                                                          final AtomicReference<Rule> internalRule,
                                                          final AtomicReference<String> internalRealmId,
                                                          final Set<String> validations,
                                                          final String requestId) {
        final Result result = Result.of(taxParam);
        result.setValidations(validations);
        result.setRuleId(internalRule.get().getId());
        result.setStatus(ResultStatus.FAIL);
        result.setRequestId(requestId);
        result.setRealmId(internalRealmId.get());
        resultService.create(result);

        return Optional.of(
                new RuleResult(result.getRequestId(), Collections.emptyMap(),
                        validations, result.getStatus())
        );
    }

    private RuleResult executeRule(final Rule rule,
                                   final TaxParam taxParam,
                                   final String realmId) {

        final Map parameters = new HashMap();
        taxParam.getDetails()
                .forEach(taxParamDetail -> parameters.putAll(taxParamDetail.toMap()));

        final Map resultMap = groovyExecutorService.execute(rule, parameters);
        final Result result = Result.of(taxParam);
        result.setRuleId(rule.getId());
        result.setStatus(ResultStatus.SUCCESS);
        result.setRealmId(realmId);

        final ObjectMapper mapper = new ObjectMapper();
        try {
            result.setRawResult(mapper.writeValueAsString(resultMap));
        } catch (JsonProcessingException e) {
            throw new BadRequestException("Error converting result values");
        }

        resultService.create(result);
        return new RuleResult(result.getRequestId(), resultMap, Collections.emptySet(), result.getStatus());
    }

    public void uploadScript(final String id, final MultipartFile file) {
        if (file == null || file.getSize() == 0 || file.getOriginalFilename() == null) {
            throw new BadRequestException("Invalid file");
        }

        if (!file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1).equals("groovy")) {
            throw new BadRequestException("Invalid file extension (Only .groovy is accepted) ");
        }

        get(id).ifPresentOrElse(rule -> readContentAndUpdate(file, rule),
                () -> {
                    throw new BadRequestException(ID_NOT_FOUND);
                });
    }

    public byte[] downloadScript(final String id) {
        return get(id)
                .map(rule -> rule.getScript().getBytes())
                .orElseThrow(() -> {
                    throw new BadRequestException(ID_NOT_FOUND);
                });
    }

    public boolean validate(final String id) {
        return get(id)
                .map(groovyExecutorService::validate)
                .orElseThrow(() -> {
                    throw new BadRequestException(ID_NOT_FOUND);
                });
    }

    private void readContentAndUpdate(MultipartFile file, Rule rule) {
        try {

            StringBuilder builder = new StringBuilder();
            try (
                    BufferedReader read = new BufferedReader(new InputStreamReader(file.getInputStream()))
            ) {
                String line;
                while ((line = read.readLine()) != null) {
                    builder.append(line).append("\n");
                }
            }

            if (builder.toString().isEmpty()) {
                throw new BadRequestException("Invalid file content ");
            }

            rule.setScript(builder.toString());
            update(rule);

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    private Optional<Rule> loadRuleByParams(final TaxParam taxParam, final ServiceTax serviceTax,
                                            final City city) {
        final Query query = new Query();
        query.addCriteria(Criteria.where("startDate")
                .lte(taxParam.getDocumentDate())
                .andOperator(
                        Criteria.where("endDate")
                                .gte(taxParam.getDocumentDate())
                ));
        query.addCriteria(Criteria.where("city").is(city.getId()));
        query.addCriteria(Criteria.where("serviceId").is(serviceTax.getId()));
        query.addCriteria(Criteria.where("active").is(Boolean.TRUE));

        final PageImpl<Rule> list = list(query, PageRequest.of(0, Integer.MAX_VALUE));

        if (list.getItems().isEmpty()) {
            throw new BadRequestException("Rule not found");
        }

        if (list.getTotalElements() > 1) {
            throw new BadRequestException("Multiple rules found. Contact our support");
        }

        return list.getItems().stream().findFirst();
    }

    private Optional<Rule> loadRule(final TaxParam taxParam,
                                    final Consumer<String> realmIdConsumer) {

        if (StringUtils.isBlank(taxParam.getServiceCode())) {
            throw new BadRequestException("Invalid serviceCode");
        }

        if (StringUtils.isBlank(taxParam.getCity())) {
            throw new BadRequestException("Invalid city");
        }

        if (StringUtils.isBlank(taxParam.getCustomerId())) {
            throw new BadRequestException("Invalid CustomerId");
        }

        if (StringUtils.isBlank(taxParam.getUserId())) {
            throw new BadRequestException("Invalid UserId");
        }

        final User user = authServiceAPI.retrieveUserById(taxParam.getUserId())
                .orElseThrow(() -> new BadRequestException("User not found"));

        final ServiceTax serviceTax = customerServiceAPI.retrieveServiceById(taxParam.getServiceCode())
                .orElseThrow(() -> new BadRequestException("ServiceCode not found"));

        final City city = addressServiceAPI.retrieveCityById(taxParam.getCity())
                .orElseThrow(() -> new BadRequestException("City not found"));

        final Customer customer = customerServiceAPI.retrieveCustomerById(taxParam.getCustomerId())
                .orElseThrow(() -> new BadRequestException("Customer not found"));

        if (!SecurityHelper.ROOT_REALM.equals(user.getRealmId()) && !customer.getRealmId().equals(user.getRealmId())) {
            throw new BadRequestException("Customer not found");
        }

        if (customer.getServiceTax().stream().noneMatch(s -> s.equals(taxParam.getServiceCode()))) {
            throw new BadRequestException(String.format("Customer doesn't have access to service code %s", taxParam.getServiceCode()));
        }

        if (customer.getCityCode().stream().noneMatch(s -> s.equals(taxParam.getCity()))) {
            throw new BadRequestException(String.format("Customer doesn't have access to city %s", taxParam.getServiceCode()));
        }

        realmIdConsumer.accept(customer.getRealmId());
        return loadRuleByParams(taxParam, serviceTax, city);
    }


}


