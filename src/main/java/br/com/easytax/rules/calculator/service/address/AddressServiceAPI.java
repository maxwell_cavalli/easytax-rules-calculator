package br.com.easytax.rules.calculator.service.address;

import br.com.easytax.iamlib.exception.BadRequestException;
import br.com.easytax.rules.calculator.domain.address.City;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.Collections;
import java.util.Optional;

@Component
public class AddressServiceAPI {

    public static final String SOMETHING_WRONG_HAPPENED = "Something wrong happened. %s";
    @Value("${services.address.endpoint}")
    private String baseURL;

    public Optional<City> retrieveCityById(final String id) {

        try {
            final WebClient client = WebClient.builder()
                    .baseUrl(baseURL)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .defaultUriVariables(Collections.singletonMap("url", baseURL))
                    .build();

            return Optional.ofNullable(
                    client.method(HttpMethod.GET)
                            .uri("/api/city/" + id)
                            .retrieve()
                            .bodyToMono(City.class)
                            .block()
            );
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new BadRequestException(String.format("City %s not found", id));
            }
            throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
        } catch (Exception e){
            throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
        }

    }

}
