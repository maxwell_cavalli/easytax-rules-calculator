package br.com.easytax.rules.calculator.service.customer;

import br.com.easytax.iamlib.exception.BadRequestException;
import br.com.easytax.rules.calculator.domain.customer.Customer;
import br.com.easytax.rules.calculator.domain.customer.ServiceTax;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.Collections;
import java.util.Optional;

@Component
public class CustomerServiceAPI {

    public static final String SOMETHING_WRONG_HAPPENED = "Something wrong happened. %s";
    @Value("${services.customer.endpoint}")
    private String baseURL;

    public Optional<Customer> retrieveCustomerById(final String id) {

        try {
            final WebClient client = WebClient.builder()
                    .baseUrl(baseURL)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .defaultUriVariables(Collections.singletonMap("url", baseURL))
                    .build();

            return Optional.ofNullable(
                    client.method(HttpMethod.GET)
                            .uri("/api/customer/" + id)
                            .retrieve()
                            .bodyToMono(Customer.class)
                            .block()
            );
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new BadRequestException(String.format("Customer %s not found", id));
            }
            throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
        } catch (Exception e){
            throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
        }
    }

    public Optional<ServiceTax> retrieveServiceById(final String id) {

        try {
            final WebClient client = WebClient.builder()
                    .baseUrl(baseURL)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .defaultUriVariables(Collections.singletonMap("url", baseURL))
                    .build();

            return Optional.ofNullable(
                    client.method(HttpMethod.GET)
                            .uri("/api/service-tax/" + id)
                            .retrieve()
                            .bodyToMono(ServiceTax.class)
                            .block()
            );
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new BadRequestException(String.format("Service %s not found", id));
            }
            throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
        } catch (Exception e){
            throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
        }
    }

}
